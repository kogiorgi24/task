<?php

	class Router {
		
		private $routes;
		private $configs;
		
		
		/**
		*	Constructor.
		*/
		public function __construct() {
			$this->configs = new Configs;

			if(file_exists(CONFIGS.'routes'.PHP)) {
				require_once CONFIGS.'routes'.PHP;

				$this->routes = $routes;
			} else {
				return false;
			}
		}
		

		/**
		*	Request url.
		*/
		private function requestUrl() {
			$requestUrl = $_SERVER['REQUEST_URI'];
			$configs = $this->configs;

			if(!empty($requestUrl)) {
				if(!empty($configs->directory)) {
					return trim($requestUrl.$configs->directory, '/');
				} else {
					return trim($requestUrl, '/');
				}
			} else {
				return false;
			}
		}
		
		
		/**
		*	Run router.
		*/
		public function run() {
			$requestUrl = $this->requestUrl();

			foreach($this->routes as $uriPattern => $path) {
				if(preg_match("~$uriPattern~", $requestUrl)) {
					$internalRoute = preg_replace("~$uriPattern~", $path, $requestUrl);
					$segments = explode('/', $internalRoute);
					
					$controllerName = array_shift($segments).'Controller';
					$actionName = 'action'.ucfirst(array_shift($segments));
					$params = $segments;
					
					$controllerObject = new $controllerName;
					$controllerResult = call_user_func_array(array($controllerObject, $actionName), $params);
					
					if($controllerResult != null)
						break;
				}
			}
		}
		
	}

?>