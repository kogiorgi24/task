<?php

	class app extends main {
		
		/**
		*	Get product list.
		*/
		public function productList($category = null) {
			$category = ($category == null) ? null : "AND products_category.title='$category'";

			$products = $this->db->customQuery("SELECT products.*, products_category.title AS category_title FROM ".$this->dbPrefix."products AS products INNER JOIN ".$this->dbPrefix."products_category AS products_category ON products.category=products_category.id WHERE products.visible='1' $category");

			if($products) {
				return $products;
			}

			return false;
		}

		/**
		*	Add Product.
		*/
		public function addProduct($items = null) {
			if(isset($items)) {
				$items = json_decode($items);
				
				$field = null;
				$value = null;

				foreach($items as $item) {
					if(empty($item->value)) {
						return 'Please fill all fields.';
						exit;
					}
					else if($item->field == 'sku') {
						if($this->checkSku($item->value) == false) {
							return 'Product sku not unique.';
						}
					}
					
					$field .= "`".$item->field."`,";
					$value .= "'".$item->value."',";
				}

				$field = substr($field, 0, -1);
				$value = substr($value, 0, -1);

				$response = $this->db->customQuery("INSERT INTO ".$this->dbPrefix."products ($field, `date`, `visible`) VALUES($value, '".date('Y-m-d H:i:s')."', '1')");

				return 'Record added successfully.';
			}
		}


		/**
		*	Check unique sku.
		*/
		private function checkSku($sku = null) {
			if(isset($sku)) {
				$response = $this->db->customQuery("SELECT sku FROM ".$this->dbPrefix."products WHERE sku='$sku'");

				if($response) {
					return false;
				}

				return true;
			}

			return false;
		}


		/**
		*	Delete Product.
		*/
		public function deleteProduct($id = null) {
			if(isset($id)) {
				$response = $this->db->customQuery("DELETE FROM ".$this->dbPrefix."products WHERE id='$id'");
				return true;
			}
		}


		/**
		*	Get product category.
		*/
		public function categoryList() {
			$category = $this->db->customQuery("SELECT * FROM ".$this->dbPrefix."products_category WHERE visible='1'");

			if($category) {
				return $category;
			}

			return false;
		}
		
	}

?>