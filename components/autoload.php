<?php

	/**
	*	Classes autoload.
	*/
	function autoloadClasses($className) {
	    try {
		    if(file_exists(COMPONENTS.$className.PHP)) {
		    	require_once COMPONENTS.$className.PHP;
		    }
		    else if(file_exists(CONFIGS.$className.PHP)) {
		    	require_once CONFIGS.$className.PHP;
		    }
		    else if(file_exists(CONTROLLERS.$className.PHP)) {
		    	require_once CONTROLLERS.$className.PHP;
		    }
		    else if(file_exists(MODELS.$className.PHP)) {
		    	require_once MODELS.$className.PHP;
		    }
		} 
		catch (Exception $exception) {
		    exit($exception->getMessage());
		}
	}

	spl_autoload_register('autoloadClasses');

?>