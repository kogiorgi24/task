<?php

	class Database {

		private $configs;
		private $pdo;
		private static $db = null;
		
		
		/**
		*	Constructor.
		*/
		public function __construct() {
			$this->configs = new Configs;

			try {
			    $this->pdo = new PDO('mysql:host='.$this->configs->dbHost.';dbname='.$this->configs->dbName.';charset=utf8', $this->configs->dbUser, $this->configs->dbPass);
			    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			    $this->pdo->exec("set names utf8");
			}
			catch(PDOException $exception) {
			    exit($exception->getMessage());
			}
		}
		
		
		/**
		*	Connection database.
		*/
		public static function connection() {
			if (self::$db == null) {
				self::$db = new Database;
				$pdo = self::$db;
			}
			
			return self::$db;
		}


		/**
		*	Database query.
		*/
		private function query($query) {
			$result = $this->pdo->prepare($query);
	        $result->execute();

	        return $result;
		}


		/**
		*	Database custom query.
		*/
		public function customQuery($query = '') {
	        if(!empty($query)) {
	            $result = $this->query($query);

	            if($result) {
	                return $result->fetchAll(PDO::FETCH_OBJ);
	            }
	        }

	        return false;
	    }

	}

?>