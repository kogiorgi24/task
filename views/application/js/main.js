$(document).ready(function() {
	
	// Add product
	$('#choose-category').on('change', function() {
		var dataId = $(this).find(':selected').data('id');
		var value = $(this).val();
		
		if(dataId == 1) {
			$('#add-product-response').html('<div class="col-md-12"><div class="form-row products-items"><div class="form-group"><input type="text" value="1" id="category" placeholder="category" hidden></div><div class="form-group col-md-3"><label for="title">Title</label><input type="text" class="form-control" id="title" placeholder="Title"></div><div class="form-group col-md-3"><label for="title">Sku</label><input type="text" class="form-control" id="sku" placeholder="Sku"></div><div class="form-group col-md-3"><label for="price">Price</label><input type="number" class="form-control" id="price" placeholder="Price"></div><div class="form-group col-md-3"><label for="size">Size</label><input type="number" class="form-control" id="size" placeholder="Size"></div><div class="form-group col-md-12"><button class="btn btn-success add-product">Add Product</button></div></div></div>');
		}
		else if(dataId == 2) {
			$('#add-product-response').html('<div class="col-md-12"><div class="form-row products-items"><div class="form-group"><input type="text" value="2" id="category" placeholder="category" hidden></div><div class="form-group col-md-3"><label for="title">Title</label><input type="text" class="form-control" id="title" placeholder="Title"></div><div class="form-group col-md-3"><label for="title">Sku</label><input type="text" class="form-control" id="sku" placeholder="Sku"></div><div class="form-group col-md-3"><label for="price">Price</label><input type="number" class="form-control" id="price" placeholder="Price"></div><div class="form-group col-md-3"><label for="weight">Weight</label><input type="number" class="form-control" id="weight" placeholder="Weight"></div><div class="form-group col-md-12"><button class="btn btn-success add-product">Add Product</button></div></div></div>');
		}
		else if(dataId == 3) {
			$('#add-product-response').html('<div class="col-md-12"><div class="form-row product-items"><div class="form-group"><input type="text" value="3" id="category" placeholder="category" hidden></div><div class="form-group col-md-3"><label for="title">Title</label><input type="text" class="form-control" id="title" placeholder="Title"></div><div class="form-group col-md-3"><label for="title">Sku</label><input type="text" class="form-control" id="sku" placeholder="Sku"></div><div class="form-group col-md-3"><label for="price">Price</label><input type="number" class="form-control" id="price" placeholder="Price"></div><div class="form-group col-md-3"><label for="height">Height</label><input type="number" class="form-control" id="height" placeholder="Height"></div><div class="form-group col-md-3"><label for="width">Width</label><input type="number" class="form-control" id="width" placeholder="Width"></div><div class="form-group col-md-3"><label for="length">Length</label><input type="number" class="form-control" id="length" placeholder="Length"></div><div class="form-group col-md-12"><button class="btn btn-success add-product">Add Product</button></div></div></div>');
		}
		else {
			$('#add-product-response').html('');
		}

		$('.add-product').on('click',function() {
			var items = [];
	        $('.form-group input').each(function(key, value) {
	            items.push({
	                field: value.id, 
	                value: value.value
	            });
	        });

	        items = JSON.stringify(items);
	        
	        $.ajax({
	            method: 'POST',
	            url: 'add',
	            dataType: 'html',
	            data: {addProduct: 'addProduct', items: items},
	            success: function(response) {
	                $('#notification-response').html(response);
	                setTimeout(function() { 
	                	$('#notification-response').empty(); 
	                }, 3000);
	            }
	        });
		});
	});

	// Remove Product
	$('.delete-item').on('click', function() {
		var id = $(this).data('id');
		var item = $(this).parent().parent();

		var result = confirm("Are you sure you want to delete this record ("+id+")?");
		if(result == true) {
			$.ajax({
	            method: 'POST',
	            url: 'products',
	            dataType: 'html',
	            data: {deleteProduct: 'deleteProduct', id: id},
	            success: function(response) {
	                if(response == true) {
	                	item.remove();
	                }
	            }
	        });
		}
	});
});