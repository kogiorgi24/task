<?php

	ini_set('display_errors', true);
	error_reporting(E_ALL);


	define('ROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);


	define('PHP', '.php');


	if(is_dir(ROOT.'components')) 
		define('COMPONENTS', realpath(ROOT.'components').DIRECTORY_SEPARATOR);


	if(is_dir(ROOT.'configs')) 
		define('CONFIGS', realpath(ROOT.'configs').DIRECTORY_SEPARATOR);


	if(is_dir(ROOT.'controllers')) 
		define('CONTROLLERS', realpath(ROOT.'controllers').DIRECTORY_SEPARATOR);


	if(is_dir(ROOT.'models')) 
		define('MODELS', realpath(ROOT.'models').DIRECTORY_SEPARATOR);


	if(is_dir(ROOT.'views')) 
		define('VIEWS', realpath(ROOT.'views').DIRECTORY_SEPARATOR);


	if(is_dir(VIEWS.'admin')) 
		define('ADMIN', realpath(VIEWS.'admin').DIRECTORY_SEPARATOR);


	if(is_dir(VIEWS.'application')) 
		define('APPLICATION', realpath(VIEWS.'application').DIRECTORY_SEPARATOR);


	session_start();


	if(file_exists(COMPONENTS.'autoload'.PHP))
		require_once COMPONENTS.'autoload'.PHP;


	$router = new Router;


	$router->run();

?>