<!DOCTYPE html>
<html>
<head>
	<title>Product List</title>
	<link rel="stylesheet" type="text/css" href="/views/application/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/views/application/css/style.css">
</head>
<body>
	
	<header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      	<h5 class="my-0 mr-md-auto font-weight-normal">Product List</h5>
      	<nav class="my-2 my-md-0 mr-md-3 menu">
        	<a class="p-2" href="/products">Products</a>
        	<a class="p-2" href="/products/discs">Discs</a>
        	<a class="p-2" href="/products/books">Books</a>
        	<a class="p-2" href="/products/furnitures">Furnitures</a>
        	<a class="p-2 text-success" href="/add">Add Product</a>
      	</nav>
    </header>

	<div class="container-fluid">
		<div class="row">
			<? if($productList): ?>
				<? foreach($productList as $product): ?>
				  	<div class="col-sm-3">
				    	<div class="card">
				      		<div class="card-body">
				        		<h5 class="card-title"><? echo $product->title; ?></h5>
				        		<? if($product->category == 1): ?>
				        			<span class="list-group">SKU: <? echo $product->sku; ?></span>
				        			<span class="list-group">Price: <? echo $product->price; ?>$</span>
				        			<span class="list-group">Size: <? echo $product->size; ?>MB</span>
				        		<? elseif($product->category == 2): ?>
				        			<span class="list-group">SKU: <? echo $product->sku; ?></span>
				        			<span class="list-group">Price: <? echo $product->price; ?>$</span>
				        			<span class="list-group">Weight: <? echo $product->weight; ?>KG</span>
				        		<? elseif($product->category == 3): ?>
				        			<span class="list-group">SKU: <? echo $product->sku; ?></span>
				        			<span class="list-group">Price: <? echo $product->price; ?>$</span>
				        			<span class="list-group">Height: <? echo $product->height; ?>, Width: <? echo $product->width; ?>, Length: <? echo $product->length; ?></span>
				        		<? endif; ?>
				        		<br/>
					        	<span class="text-danger delete-item" data-id="<? echo $product->id; ?>">Delete</span>
				      		</div>
				    	</div><br/>
				  	</div>
				<? endforeach; ?>
			<? endif; ?>
		</div>
	</div>

	<script type="text/javascript" src="/views/application/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/views/application/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/views/application/js/main.js"></script>
</body>
</html>