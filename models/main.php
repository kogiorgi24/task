<?php

	class main {
		
		protected $db;
		protected $dbPrefix;
		protected $configs;


		/**
		*	Constructor.
		*/
		public function __construct() {
			$this->configs = new Configs;
			$this->dbPrefix = $this->configs->dbPrefix;
			$this->db = new Database;
		}
		
	}

?>