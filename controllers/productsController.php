<?php

	class productsController {
		
		/**
		*	Product list.
		*/
		public function actionList($category = null) {
			$app = new App;
			$productList = $app->productList($category);

			if(isset($_POST['deleteProduct'])) {
				print $app->deleteProduct($_POST['id']);
				exit;
			}

			if(file_exists(APPLICATION.'products'.PHP))
				require_once APPLICATION.'products'.PHP;
			return true;
		}
		

		/**
		*	Add product.
		*/
		public function actionAdd($category = null) {
			$app = new App;
			$categoryList = $app->categoryList();

			if(isset($_POST['addProduct'])) {
				print $app->addProduct($_POST['items']);
				exit;
			}

			if(file_exists(APPLICATION.'add'.PHP))
				require_once APPLICATION.'add'.PHP;
			return true;
		}
		
	}

?>