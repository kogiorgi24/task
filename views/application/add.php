<!DOCTYPE html>
<html>
<head>
	<title>Add Product</title>
	<link rel="stylesheet" type="text/css" href="/views/application/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/views/application/css/style.css">
</head>
<body>
	<header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      	<h5 class="my-0 mr-md-auto font-weight-normal">Add Product</h5>
      	<nav class="my-2 my-md-0 mr-md-3 menu">
        	<a class="p-2" href="/products">Products</a>
      	</nav>
    </header>

	<div class="container-fluid">
		<div class="form-group col-md-3">
			<label for="choose-category">Choose Category</label>
		    <select class="form-control" id="choose-category">
		      	<option selected disabled hidden>Choose Category</option>
		      	<? foreach($categoryList as $list): ?>
			      	<option data-id="<? echo $list->id; ?>"><? echo $list->title; ?></option>
			    <? endforeach; ?>
		    </select>
		</div>

		<div id="add-product-response"></div>
		<div class="text-center" id="notification-response"></div>
	</div>

	<script type="text/javascript" src="/views/application/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/views/application/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/views/application/js/main.js"></script>
</body>
</html>