-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 30, 2019 at 01:59 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_products`
--

CREATE TABLE `db_products` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `size` decimal(10,0) DEFAULT NULL,
  `weight` decimal(10,0) DEFAULT NULL,
  `height` decimal(10,0) DEFAULT NULL,
  `width` decimal(10,0) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `date` datetime NOT NULL,
  `visible` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_products`
--

INSERT INTO `db_products` (`id`, `category`, `sku`, `title`, `price`, `size`, `weight`, `height`, `width`, `length`, `date`, `visible`) VALUES
(1, 1, 'AZ3219', 'Disc', '26', '25', NULL, NULL, NULL, NULL, '2019-12-23 03:04:05', 1),
(2, 2, 'DC512G', 'Book', '50', NULL, '2', NULL, NULL, NULL, '2019-12-23 03:12:10', 1),
(3, 3, 'ABC342', 'Furniture', '500', NULL, NULL, '110', '120', '130', '2019-12-23 03:04:05', 1),
(4, 1, 'MNV543', 'Disc 2', '36', '15', NULL, NULL, NULL, NULL, '2019-12-23 03:04:05', 1),
(5, 2, 'LKJ564', 'Book 2', '40', NULL, '2', NULL, NULL, NULL, '2019-12-23 03:12:10', 1),
(6, 3, 'POI761', 'Furniture 2', '800', NULL, NULL, '150', '160', '170', '2019-12-23 03:04:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_products_category`
--

CREATE TABLE `db_products_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `visible` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_products_category`
--

INSERT INTO `db_products_category` (`id`, `title`, `date`, `visible`) VALUES
(1, 'Discs', '2019-12-23 03:03:04', 1),
(2, 'Books', '2019-12-23 03:03:04', 1),
(3, 'Furnitures', '2019-12-23 03:03:04', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `db_products`
--
ALTER TABLE `db_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `db_products_category`
--
ALTER TABLE `db_products_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `db_products`
--
ALTER TABLE `db_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `db_products_category`
--
ALTER TABLE `db_products_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
